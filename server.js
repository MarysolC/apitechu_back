
var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
require('dotenv').config();
const cors = require ('cors'); //clase 17.10.2019
var port = process.env.PORT || 3000;
var URL_BASE="/techu-peru/v1/";

var baseMlabURL = 'https://api.mlab.com/api/1/databases/techu35db/collections/';
const apikeyMLab = 'apiKey='+ process.env.MLAB_API_KEY;

const users_controller = require('./controllers/users_controller'); //agregue
//const URL_BASE = process.env.'URL_BASE';

app.use(bodyParser.json());

app.use(cors()); //agregado 17.10.19
app.options('*', cors()); //agregado 17.10.19

//app.get(URL_BASE + 'users', users_controller.getUsers);

app.listen(port, function () {
 console.log('Example app listening on port 3000!');
   });


// GET users consumiendo API REST de mLab 09.10.2019
app.get(URL_BASE + 'users/:id/accounts', // 'users/:id'
 function(req, res) {
   console.log("GET /techu-peru/v1/users");
   console.log(req.params.id);
   var id = req.params.id;
   var queryString = 'q={"id":' + id + '}&';
   var queryStrField = 'f={"_id":0}&';
   var httpClient = requestJSON.createClient(baseMlabURL);
   console.log("Cliente HTTP mLab creado.");

   httpClient.get('Account?' + queryString + queryStrField + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

//POST users con Mlab   ----CLASE 10.10
app.post(URL_BASE + 'users',
 function(req, res){
   var clienteMlab = requestJSON.createClient(baseMlabURL);
   console.log(req.body);
   clienteMlab.get('user?' + apikeyMLab,
 function(err,respuestaMLab, body){
   newID = body.length + 1;
   console.log("newID:" + newID);
   var newUser ={
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "password" : req.body.password
   };
 clienteMlab.post(baseMlabURL + "user?" + apikeyMLab, newUser,
  function(err, respuestaMLab, body){
    console.log(body);
    res.status(201);
    res.send(body);
    });
  });
});

// //PUT users con parámetro 'id'  ----CLASE 10.10
// app.put(URL_BASE + 'users/:id',
// function(req, res) {
//  var id = req.params.id;
//  var queryStringID = 'q={"id":' + id + '}&';
//  var clienteMlab = requestJSON.createClient(baseMLabURL);
//  clienteMlab.get('user?'+ queryStringID + apikeyMLab,
//    function(err, respuestaMLab, body) {
//     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
//     console.log(req.body);
//     console.log(cambio);
//     clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
//      function(err, respuestaMLab, body) {
//        console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
//       //res.status(200).send(body);
//       res.send(body);
//      });
//    });
// });

// Petición PUT con id de mLab (_id.$oid) ----CLASE 10.10
 app.put(URL_BASE + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });

 // //DELETE user with id -----14.10.2019
 // app.delete(URL_BASE + "users/:id",
 //  function(req, res){
 //    var id = req.params.id;
 //    var queryStringID = 'q={"id":' + id + '}&';
 //    console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
 //    var httpClient = requestJSON.createClient(baseMLabURL);
 //    httpClient.get('user?' +  queryStringID + apikeyMLab,
 //      function(err, respuestaMLab, body){
 //        var respuesta = body[0];
 //        httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
 //          function(err, respuestaMLab,body){
 //            res.send(body);
 //        });
 //      });
 //  });

  //Login con POST   ----CLASE 14.10
  app.post(URL_BASE + 'login',
     function(req, res){
       console.log("POST/apitechu/v1/login");
       let email = req.body.email, pass=req.body.password;

       let queryString = 'q={"email": "'+email+'","password":"'+pass+'"}&';
       let limFilter='l=1&';
       let clienteMlab= requestJSON.createClient(baseMlabURL);
       console.log('user?' + queryString + limFilter + apikeyMLab);
       clienteMlab.get ('user?' + queryString + limFilter + apikeyMLab,
       function(err, respuestaMLab, body) {
         console.log(body);
         if(!err){
           if(body.length==1){
             let login= '{"$set":{"logged":true}}';
             console.log("ingresa a login");
             let stringQueryPut = body[0]._id.$oid + '?';
              console.log('user/'+ stringQueryPut + apikeyMLab);
             clienteMlab.put('user/'+ stringQueryPut + apikeyMLab, JSON.parse(login),
             function(errPut,resPut, bodyPut){
               res.send({'msg':'Login correcto','user':body[0].email, 'userid':body[0].id});
            });
          }
          else{
            response = {"msg" : "Ningún elemento 'user'."};
            res.status(404).send(response);
          }
        }
// app.get(URL_BASE + 'users',
//   function (req, res) {
//     res.status(201).send(user_file);
      })
});

app.get(URL_BASE + 'users/:id/users',//Peticion de GET  on ID
 function (req, res) {
   let pos = req.params.id - 1;
   console.log('GET con id = ' + req.params.id);
   let longitud = user_file.length;
   console.log(longitud);
   let respuesta = (user_file[pos] == undefined) ?
     {"msg":"usuario no encontrado"} : user_file[pos];
   res.send(respuesta);
});

//Peticion de GET  on String
// app.get(URL_BASE + 'users',
//   function (req, res) {
//     console.log('GET con query String');
//     console.log(req.query.id);
//     console.log(req.query.name);
// });

//POST users
app.post(URL_BASE + 'users',
 function(req, res){
   console.log('POST de users');
   // console.log('Nuevo usuario: ' + req.body);
   // console.log('Nuevo usuario: ' + req.body.first_name);
   // console.log('Nuevo usuario: ' + req.body.email);
   let newID = user_file.length +1;
   let newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "password" : req.body.password
   };
   user_file.push(newUser);
   console.log("nuevo usuario: " + newUser);
   res.send(newUser);
});

//PUT users
app.put(URL_BASE + 'users/:id',
 function(req, res){
   console.log('PUT de users');
   let pos = req.body.id - 1;
   let newUser = {
     "id" : req.params.id,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "password" : req.body.password
   };
   user_file[pos] = newUser;
   console.log("nuevo usuario: " + JSON.stringify(newUser));
   res.send(newUser);
});

//DELETE users
app.delete(URL_BASE + 'users/:id',
function(req, res){
  console.log('Delete de users. ');
  let pos = req.params.id;
  user_file.splice(pos - 1, 1);
  res.send({"msg":"Usuario eliminado"});
});

//Login
app.post(URL_BASE + 'Login',
function(req, res){
  console.log('login');
  console.log(req.body.email);
  console.log(req.body.password);
  let tam = user_file.length;
  let i=o;
  let encontrado = false;
  while((i< user_file.length) && !encontrado){

    if(user_file[i].email == email && user_file[i].password == password)
    {
      encontrado = true;
      user_file[i].logged = true;
    }
    i++;
  }
  if(encontrado)
    res.send({"encontrado":"si", "id":i})
  else{
    res.send({"encontrado":"no"})
}

});

//LOGOUT users
app.post(global.URL_BASE + 'login/:id',
  function (req, res) {
    let paramID = req.params.id;
    if (paramID == 'logued'){
      let i = 0;
      let encontrado = false;
      let respuesta = {
                        table: []
                      };
      while (i < user_file.length) {
        if (user_file[i].logged){
          respuesta.table.push(user_file[i]);
        }
        i++;
      }
      var json = JSON.stringify(respuesta);
      res.send(respuesta);

    } else {
      let EstuvoLogueado = false;
      let i = 0;
      let encontrado = false;
      let idUsuario = 0;
      while (i < user_file.length && !encontrado) {
        if (user_file[i].ID == paramID) {
          if (user_file[i].logged)  {
            user_file[i].logged = false;
            EstuvoLogueado = true;
          }
          idUsuario = user_file[i].ID
          encontrado = true;
        }
        i++;
      }
      if (encontrado){ //fue encontrado
        console.log('Fue encontrado');
        if (EstuvoLogueado){ //estuvo logueado
          console.log('Estuvo logueado');
          res.send({"Usuario deslogueado":"Si","id":user_file[i-1]})
        } else
            res.send({"Usuario logueado previamente":"No","id":user_file[i-1]})
      } else
          res.send({"Usuario encontrado":"No"})
    }
});

/*function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }*/
