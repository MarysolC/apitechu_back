var express = require('express');
var user_file = require('../user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
const URL_BASE = '/techu-peru/v1/'
app.use(bodyParser.json());
//
// app.get(URL_BASE + 'users',
//   function (req, res) {
//     res.status(201).send(user_file);
// });

//Peticion de GET  on ID
//app.get(URL_BASE + 'users/:id', //crear objeto app
 function getUsers (req, res) { // modificado
   let pos = req.params.id - 1;
   console.log('GET con id = ' + req.params.id);
   let longitud = user_file.length;
   console.log(longitud);
   let respuesta = (user_file[pos] == undefined) ?
     {"msg":"usuario no encontrado"} : user_file[pos];
   res.send(respuesta);
};

module.exports ={
  getUsers
}
